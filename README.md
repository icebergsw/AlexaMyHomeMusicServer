## Synopsis

Provides a default server implementation that can be run on a home server in support of the Alexa My Home Music skill. This project allows for serving music off one's home machine to an Amazon Echo device.

This code provides a functional server that can be extended to support existing music organization APIs, were one so inclined. The default implementation is a simple implementation that allows for music to be served to the My Home Music Alexa skill.

See the [My Home Music help page](https://icebergsw.com/alexa/myhomemusic/mhmhelp.html) for additional information. 

## Code Example

This project requires a Java 8 runtime and the Apache tika libraries to run.

Assuming a runnable jar, execution is simply:

    java -jar mhmserver.jar

A configuration file (see the exampleconfig.xml) is required for configuration and is expected in the current working directory. The location of the configuration file may also be set via command-line:

    java -jar mhmserver.jar -config /path/to/amsconfig.xml

Alternatively put the amsconfig.xml in the user's home directory.

Port forwarding must be allowed to the application, and the application must be configured to serve an https endpoint (self-signed certificate is acceptable).

## Motivation

Amazon Echo devices are designed to play music. There are generally these existing models:

1. Play music from your Amazon cloud library over your Echo device.
2. Stream music from an existing music streaming service to which you have an account over your Echo device.
3. Control your home audio configuration via voice-commands from Echo to play your music via your home entertainment system (smart home skills linking).

What is missing is:

4. Play your own music, from your own home server, over the Echo device.

The My Home Music application is intended to fill this last part. This project provides the server implementation that runs on your home device to enable that integration.

## Installation

The latest runnable jar file is available at: https://icebergsw.com/alexa/myhomemusic/mhmserver.jar

The SHA-256 hash of the server JAR file is at: https://icebergsw.com/alexa/myhomemusic/mhmserver.txt

Download the runnable JAR into a desired folder. Within that same folder, create a configuration file based on the sampleconfig.xml, essentially:

    <?xml version="1.0" encoding="UTF-8"?>
    <configuration>
	   <port></port>
	   <keystorePath>keystore.jks</keystorePath>
	   <keystorePassword>password</keystorePassword>
	   <searchPaths>
		  <path></path>
	   </searchPaths>
	   <musicPlayerClass>com.iceberg.alexa.music.server.player.CorePlayer</musicPlayerClass>
	   <authToken></authToken>
	   <cacheFile></cacheFile>
    </configuration>

The configuration file should be in the same folder as the runnable JAR and should be named: amsconfig.xml.

Populate the configuration file with the relevant values for your server.

To create a self-signed certificate, run keytool:

    keytool -genkey -keyalg RSA -alias selfsigned -keystore keystore.jks -storepass password -validity 360 -keysize 2048

You should change both the keystore password and the password in the configuration file to something other than "password".

At least one search path must be defined. Provided paths are searched recursively.

The port must be defined in the configuration file (any non-reserved port may be used).

You may also need to configure port forwarding via your home router.

### Building from Sources

To build yourself, clone the project and compile as a runnable jar file including the Apache tika app library.

You will need to create a configuration file from the sample and a keystore as noted.

## API Reference

This is a running application, not an API. However, the API is extensive via the MusicPlayer interface.

See the [My Home Music help page](https://icebergsw.com/alexa/myhomemusic/mhmhelp.html) for additional information about the runtime endpoints that must be available.

## Tests

Describe and show how to run the tests with code examples.

## Contributors

The core server implementation must remain in sync with the Alexa My Home Music skill implementation.

Thus the best approach is to extend to additional implementations via the MusicPlayer interface (add new implementations).

## License

This software is free-to-use and free-to-modify and otherwise free to do with as one pleases. I haven't (yet) picked a specific license implementation for it.
