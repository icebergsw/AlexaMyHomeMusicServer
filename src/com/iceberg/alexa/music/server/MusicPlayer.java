package com.iceberg.alexa.music.server;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Map;

/**
 * This is the interface that any custom music player implementation has to implement.
 *
 * For all methods, the callback is defined below (in the callback inner class).
 * 
 * The filters map is query parameters from the http request. Typical query parameters
 * will be of the form:
 * 		genre=<value>
 * 		artist=<value>
 * 		album=<value>
 * 		song=<value>
 * 		playlist=<value>
 */
public interface MusicPlayer
{
	/**
	 * Called immediately after the music player is created to carry along
	 * any configuration data and for the player to perform any necessary
	 * initialization.
	 * 
	 * @param conf
	 */
	public void initialize(Configuration conf);
	
	/**
	 * Plays the next random song. This should always return something.
	 */
	public void playRandom(MusicPlayerCallback cb, Map<String, String> filters);
	
	/**
	 * Plays the next song in the current playlist (or random, or previous, or whatever).
	 * If a sequence number (seq=# in the URL, seq in filters) is provided, play that.
	 * 
	 * This can't always be done reliably, unfortunately. For next and previous,
	 * those aren't given in a context where we can reliably get the number (at least
	 * without storing data in a database on the lambda side, which we don't want to do).
	 * 
	 * So we use a couple special query parameters:
	 *  cmd=next => next song
	 *  cmd=previous => previous song
	 * 
	 * Note that, for next, the "next" song is almost always requested by the Alexa
	 * service immediately upon the "current" song playing. So next usually means
	 * to play the last song requested (which will feel like a repeat in most cases,
	 * from the point of view of the server). So one can actually treat "next" as
	 * "repeat the last song returned" and "previous" as "go back one".
	 */
	public void playNext(MusicPlayerCallback cb, Map<String, String> filters);
	
	/**
	 * This will generally be called first, to start playback, or a particular
	 * filtered type of music, such as song or genre or album. This function
	 * should always check the filters for the type of music. If the player
	 * queues music, this function is the best one to populate the queue.
	 */
	public void playFiltered(MusicPlayerCallback cb, Map<String, String> filters);
	
	/**
	 * Used for the player to pass data back to the music service.
	 * 
	 * This is, primarily, a wrapper to hide the http implementation
	 * from the music player.
	 */
	public interface MusicPlayerCallback
	{
		/**
		 * Must be called to set the length of the audio track being returned.
		 * @param bytes The length of the audio track in bytes.
		 */
		public void setAudioLength(long bytes) throws IOException;
		
		/**
		 * The output stream should be populated with the song. Either this
		 * method or the streamFile() method should be used, but not both.
		 * @return The OutputStream to which the file content should be sent.
		 */
		public OutputStream getOutputStream();
		
		/**
		 * Sends the given file to the server (Alexa) as the audio file.
		 * Either this or getOutputStream() should be used, but not both.
		 * @param path The path to the file to play.
		 */
		public void streamFile(File path) throws IOException;
	}
}
