package com.iceberg.alexa.music.server;

import java.io.IOException;
import java.util.Map;

import com.sun.net.httpserver.Filter;
import com.sun.net.httpserver.HttpExchange;

/**
 * Filter that processes incoming http requests.
 * We look for a query parameter of "authToken=<value>" and make
 * sure that value matches our expected token. If it does not
 * we reject the request.
 * 
 * Because the token is only known to AWS, and all requests are https,
 * we're really counting on AWS to keep the token secure.
 *
 * Note that if the token is empty or null, this filter should never
 * even be created (that check is handled outside this class).
 */
public class TokenFilter extends Filter {

	private static final String AUTH_TOKEN_PARAMETER = "authToken";
	
	private String expectedToken;
	
	public TokenFilter(String token) {
		this.expectedToken = token;
	}

	@Override
	public String description() {
		return "Ensures the calling service passes a valid Alexa token.";
	}

	@Override
	public void doFilter(HttpExchange exchange, Chain filterChain) throws IOException {
		Map<String, String> queryParameters = MusicServer.getQueryParametersFromRequest(exchange);
		
		String tokenValue = queryParameters.get(AUTH_TOKEN_PARAMETER);
		
		if ((tokenValue != null) && (tokenValue.equals(expectedToken)))
		{
			filterChain.doFilter(exchange);
		}
		else
		{
			System.err.println("Failed authentication, expected: [" + expectedToken + "], received: [" + tokenValue + "]");
			failExchangeAsInvalidAuthentication(exchange);
		}
	}

	private void failExchangeAsInvalidAuthentication(HttpExchange exchange) throws IOException {
		exchange.sendResponseHeaders(401, 0L);	// 401 = unauthorized
		exchange.getResponseBody().close();
	}

}
