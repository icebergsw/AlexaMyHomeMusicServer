package com.iceberg.alexa.music.server;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Contains configuration information.
 * This defines the valid entries in an amsconfig.xml file.
 * 
 * See the example file for configuration file example.
 * This and the example file should be kept in sync.
 */
@XmlRootElement(name="configuration")
@XmlAccessorType(XmlAccessType.FIELD)
public class Configuration
{
	private String port;
	private String keystorePath;
	private String keystorePassword;
	private String musicPlayerClass;
	private SearchPathList searchPaths;
	private String authToken;
	private String cacheFile;
	
	public String getKeystorePath() {
		return keystorePath;
	}

	public void setKeystorePath(String keystorePath) {
		this.keystorePath = keystorePath;
	}

	public String getKeystorePassword() {
		return keystorePassword;
	}

	public void setKeystorePassword(String keystorePassword) {
		this.keystorePassword = keystorePassword;
	}
	
	public String getMusicPlayerClass() {
		return musicPlayerClass;
	}

	public void setMusicPlayerClass(String musicPlayerClass) {
		this.musicPlayerClass = musicPlayerClass;
	}

	public SearchPathList getSearchPaths() {
		return searchPaths;
	}

	public void setSearchPaths(SearchPathList searchPaths) {
		this.searchPaths = searchPaths;
	}

	public List<String> getPaths()
	{
		return searchPaths.getSearchPaths();
	}
	
	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public int getPortNum() {
		return Integer.parseInt(port);
	}
	
	public String getAuthToken() {
		return authToken;
	}

	public void setAuthToken(String authToken) {
		this.authToken = authToken;
	}
	
	public String getCacheFile() {
		return cacheFile;
	}

	public void setCacheFile(String cacheFile) {
		this.cacheFile = cacheFile;
	}

	@XmlRootElement(name="searchPaths")
	@XmlAccessorType(XmlAccessType.FIELD)
	public static class SearchPathList
	{
		@XmlElement(name="path")
		private List<String> searchPaths = null;
		
		public List<String> getSearchPaths() {
			return searchPaths;
		}
		
		public void setSearchPaths(List<String> searchPaths) {
			this.searchPaths = searchPaths;
		}

	};
}
