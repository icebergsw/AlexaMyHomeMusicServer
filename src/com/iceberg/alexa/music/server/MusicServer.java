package com.iceberg.alexa.music.server;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URLDecoder;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;

import com.iceberg.alexa.music.server.player.CorePlayer;
import com.sun.net.httpserver.Filter;
import com.sun.net.httpserver.HttpContext;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

/**
 * This is the default music server.
 * It sets up an https server on the local machine.
 * 
 * The player is defined in the configuration. This delegates
 * calls to the defined player.
 */
public class MusicServer {

	private Configuration conf;
	private HttpServer server;
	private MusicPlayer player;
	private Filter authenticationFilter = null;
	
	public MusicServer()
	{
		
	}
	
	public void setConfiguration(Configuration conf) {
		this.conf = conf;
	}

	public void start() throws Exception {
		
		createAndInitializeMusicPlayer();
		
		createAuthenticationFilter();
		
		createAndStartServer();
		
	}

	private void createAndInitializeMusicPlayer() {
		createCustomMusicPlayer();
		
		if (player == null)
		{
			// create built-in
			player = new CorePlayer();
		}
		
		player.initialize(conf);
	}

	/**
	 * Creates the instance of the player defined
	 * in the configuration file.
	 */
	private void createCustomMusicPlayer() {
		try
		{
			String className = conf.getMusicPlayerClass();
			if (className != null)
			{
				Class<?> klass = Class.forName(className);
				MusicPlayer mp = (MusicPlayer)klass.newInstance();
				
				player = mp;
			}
		}
		catch (Exception ex)
		{
			System.err.println("Failed to create custom music player: " + ex.getMessage());
		}
	}

	/**
	 * If the configuration specifies a valid token, create a filter for that.
	 */
	private void createAuthenticationFilter() {
		String token = conf.getAuthToken();
		
		if ((token != null) && (token.trim().length() > 0))
		{
			authenticationFilter = new TokenFilter(token);
		}
	}

	/**
	 * Sets up the valid server endpoints.
	 * 
	 * The withFilters call will add a filter to check for a valid (matching)
	 * authentication token if an authToken is specified in the configuration,
	 * 
	 * The authToken comes from the Alexa account linking and can be retrieved
	 * from the Alexa service via a card in the alexa app:
	 * 	"Alexa, ask my home music for my token"
	 * 
	 * Note that the commands do not have an auth token. While we probably can
	 * get one, the way some of the playback controls work, it's not an easy
	 * default for that to happen, so it's not included at this time.
	 * 
	 * Given that the default My Home Music skill can't even call them (without
	 * lambda being configured with internet access), one might prefer to
	 * disable the commands entirely.
	 * 
	 * @throws Exception
	 */
	private void createAndStartServer() throws Exception {
		server = SSLTools.createHttpsServer(conf);
		
		withFilters(server.createContext("/randomsong.mp3", new HttpHandler() {
			@Override
			public void handle(HttpExchange exchange) throws IOException {
				player.playRandom(new MPCBInterface(exchange), getQueryParametersFromRequest(exchange));
			}
		}));
		
		withFilters(server.createContext("/filteredsong.mp3", new HttpHandler() {
			@Override
			public void handle(HttpExchange exchange) throws IOException {
				player.playFiltered(new MPCBInterface(exchange), getQueryParametersFromRequest(exchange));
			}
		}));

		withFilters(server.createContext("/nextsong.mp3", new HttpHandler() {
			@Override
			public void handle(HttpExchange exchange) throws IOException {
				player.playNext(new MPCBInterface(exchange), getQueryParametersFromRequest(exchange));
			}
		}));
		
		// shuffle can be "on" or "off"
		server.createContext("/command/shuffle/", new HttpHandler() {
			@Override
			public void handle(HttpExchange exchange) throws IOException {
				System.out.println("shuffle set to: " + exchange.getRequestURI());
				sendEmptyOK(exchange);
			}
		});
		
		// loop can be "on" or "off"
		server.createContext("/command/loop/", new HttpHandler() {
			@Override
			public void handle(HttpExchange exchange) throws IOException {
				System.out.println("loop set to: " + exchange.getRequestURI());
				sendEmptyOK(exchange);
			}
		});

		server.createContext("/command/repeat", new HttpHandler() {
			@Override
			public void handle(HttpExchange exchange) throws IOException {
				System.out.println("repeat called as: " + exchange.getRequestURI());
				sendEmptyOK(exchange);
			}
		});
		
		server.createContext("/command/startover", new HttpHandler() {
			@Override
			public void handle(HttpExchange exchange) throws IOException {
				System.out.println("startover called as: " + exchange.getRequestURI());
				sendEmptyOK(exchange);
			}
		});
		
		withFilters(server.createContext("/ping", new HttpHandler() {
			@Override
			public void handle(HttpExchange exchange) throws IOException {
				System.out.println("pinged at: " + exchange.getRequestURI());
				sendEmptyOK(exchange);
			}
		}));

		server.start();
	}

	protected void sendEmptyOK(HttpExchange exchange) throws IOException {
		exchange.sendResponseHeaders(200, 0);
		OutputStream os = exchange.getResponseBody();
		os.close();
	}

	/**
	 * Adds an authentication filter if one exists.
	 * 
	 * @param createContext
	 */
	private void withFilters(HttpContext context) {
		if (authenticationFilter != null)
		{
			context.getFilters().add(authenticationFilter);
		}
	}

	public static Map<String, String> getQueryParametersFromRequest(HttpExchange exchange) {
		Map<String, String> filters = new HashMap<String, String>();
		
		String query = exchange.getRequestURI().getRawQuery();
		if (query != null)
		{
			String[] queryParts = query.split("&");
			for (String queryPart : queryParts)
			{
				try
				{
					String[] keyValue = queryPart.split("=");
					if (keyValue.length == 2)
					{
						String key = keyValue[0];
						String value = URLDecoder.decode(keyValue[1], "UTF-8");
						
						filters.put(key, value.toLowerCase());
					}
					// otherwise it's not valid so ignore it
				}
				catch (Exception ex)
				{
					System.err.println("Invalid http query parameter: " + ex.getMessage());
					// ignore it, move on to next parameter
				}
			}
		}
		
		return filters;
	}

	/**
	 * Music Player Callback interface implementation.
	 *
	 */
	public static class MPCBInterface implements MusicPlayer.MusicPlayerCallback
	{
		private HttpExchange exchange;
		
		public MPCBInterface(HttpExchange exchange)
		{
			this.exchange = exchange;
		}
		
		@Override
		public void setAudioLength(long bytes) throws IOException {
			exchange.sendResponseHeaders(200, bytes);
		}

		@Override
		public OutputStream getOutputStream() {
			return exchange.getResponseBody();
		}

		@Override
		public void streamFile(File audioFile) throws IOException {
			OutputStream os = exchange.getResponseBody();
			Path path = audioFile.toPath();
			Files.copy(path, os);
			os.flush();
			os.close();
		}
		
	}
}
