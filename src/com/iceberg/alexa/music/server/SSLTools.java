package com.iceberg.alexa.music.server;

import java.io.*;
import java.net.InetSocketAddress;
import java.security.*;

import javax.net.ssl.*;

import com.sun.net.httpserver.HttpServer;
import com.sun.net.httpserver.HttpsConfigurator;
import com.sun.net.httpserver.HttpsParameters;
import com.sun.net.httpserver.HttpsServer;

/**
 * This is a set of functions to set up an https server, to perform the SSL config
 * for such a server.
 * 
 * The keystore and password to the keystore must be defined in the
 * configuration file.
 * 
 * Alexa requires that music URLs (mp3 URLs) are encrypted via https, but
 * they don't require that it's a trusted cert; in this case self-signed
 * certificates are fine.
 *
 */
public class SSLTools {

	public static HttpServer createHttpsServer(Configuration conf) throws Exception {
		HttpsServer server = HttpsServer.create(new InetSocketAddress(conf.getPortNum()), 0);
		
		SSLContext sslContext = createSSLContext(conf);
		HttpsConfigurator configurator = getConfigurator(sslContext);
		server.setHttpsConfigurator(configurator);
		
		server.setExecutor(null);// create default
		
		return server;
	}

	/**
	 * This method is really for testing, so we can try running without HTTPS.
	 * @param conf
	 * @return
	 * @throws IOException 
	 */
	public static HttpServer createHttpServer(Configuration conf) throws IOException {
		HttpServer server = HttpServer.create(new InetSocketAddress(conf.getPortNum()), 0);
		server.setExecutor(null);
		return server;
	}

	private static SSLContext createSSLContext(Configuration conf) throws Exception {
		FileInputStream keyStoreStream = new FileInputStream(conf.getKeystorePath()); 
		KeyStore keystore = KeyStore.getInstance("JKS");
		keystore.load(keyStoreStream, conf.getKeystorePassword().toCharArray());
		KeyManagerFactory kmf = KeyManagerFactory.getInstance("SunX509");
		kmf.init(keystore, conf.getKeystorePassword().toCharArray());
		TrustManagerFactory tmf = TrustManagerFactory.getInstance("SunX509");
		tmf.init(keystore);
		SSLContext sslContext = SSLContext.getInstance("TLS");
		sslContext.init(kmf.getKeyManagers(), tmf.getTrustManagers(), null);
		return sslContext;
	}

	private static HttpsConfigurator getConfigurator(SSLContext sslContext) {
		return new HttpsConfigurator(sslContext) {
			@Override
			public void configure(HttpsParameters params) {
				final SSLContext context = getSSLContext();
				final SSLParameters sslParams = context.getDefaultSSLParameters();
				params.setNeedClientAuth(false);
				params.setSSLParameters(sslParams);
			}
		};
	}

}
