package com.iceberg.alexa.music.server.player;

import javax.xml.bind.annotation.*;

/**
 * A cache entry stores information about a song in the cache.
 * Loading from cache is a whole lot faster than trying to parse
 * every file on startup every time.
 *
 */
@XmlRootElement(name="song")
@XmlAccessorType(XmlAccessType.FIELD)
public class CacheEntry
{
	public CacheEntry() {}
	public CacheEntry(String path, String artist, String title, String album, String genre)
	{
		this.path = path;
		this.title = title;
		this.artist = artist;
		this.album = album;
		this.genre = genre;
	}
	
	private String path;
	private String title;
	private String artist;
	private String album;
	private String genre;
	
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getArtist() {
		return artist;
	}
	public void setArtist(String artist) {
		this.artist = artist;
	}
	public String getAlbum() {
		return album;
	}
	public void setAlbum(String album) {
		this.album = album;
	}
	public String getGenre() {
		return genre;
	}
	public void setGenre(String genre) {
		this.genre = genre;
	}
	
}
