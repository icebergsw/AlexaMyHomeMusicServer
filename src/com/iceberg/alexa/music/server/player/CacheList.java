package com.iceberg.alexa.music.server.player;

/**
 * The list entry for JAXB for the cache list/entry.
 */
import java.util.*;

import javax.xml.bind.annotation.*;

@XmlRootElement(name="songs")
@XmlAccessorType(XmlAccessType.FIELD)
public class CacheList {

	@XmlElement(name="song")
	private List<CacheEntry> songs = new ArrayList<CacheEntry>();
	
	public List<CacheEntry> getSongs() {
		return songs;
	}
	
	public void setSongs(List<CacheEntry> songs) {
		this.songs = songs;
	}
}
