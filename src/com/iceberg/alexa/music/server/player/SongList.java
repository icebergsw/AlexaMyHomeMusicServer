package com.iceberg.alexa.music.server.player;

import java.io.*;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.*;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.Parser;
import org.apache.tika.parser.mp3.Mp3Parser;
import org.xml.sax.ContentHandler;
import org.xml.sax.helpers.DefaultHandler;

import com.iceberg.alexa.music.server.Configuration;

/**
 * Used by the CorePlayer to store the list of songs on the user's machine.
 * Also organizes the songs by genre, artist, album, and song, in case the
 * user wants to play by those groups.
 *
 */
public class SongList {

	private static SongList instance = null;
		
	private List<String> filePaths = new ArrayList<String>();
	
	private CacheList cache;
	
	private Map<String, List<String>> artists = new HashMap<>();
	private Map<String, List<String>> songs = new HashMap<>();
	private Map<String, List<String>> albums = new HashMap<>();
	private Map<String, List<String>> genres = new HashMap<>();
	
	private Random mRandom = new Random();
	
	public synchronized static SongList getInstance(Configuration conf) {
		if (instance == null)
		{
			instance = new SongList();
			instance.initialize(conf);
		}
		
		return instance;
	}

	private void initialize(Configuration conf) {
		loadFromCache(conf.getCacheFile());
		loadSongsFromAllPaths(conf.getPaths());
		writeCache(conf.getCacheFile());
	}

	public File getNextRandom() {
		return new File(filePaths.get(mRandom.nextInt(filePaths.size())));
	}

	private void loadSongsFromAllPaths(List<String> paths) {
		for (String path : paths)
		{
			try
			{
				loadSongsFromPath(path);
			}
			catch (Exception ex)
			{
				System.err.println(ex.getMessage());
			}
		}
	}

	private void loadSongsFromPath(String path) throws IOException {
		Files.walkFileTree(Paths.get(path), new FileVisitor<Path>() {

			@Override
			public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
				return FileVisitResult.CONTINUE;
			}

			@Override
			public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
				if (isMP3File(file))
				{
					addSong(file);
				}
				return FileVisitResult.CONTINUE;
			}

			private boolean isMP3File(Path file) {
				return file.toString().endsWith("mp3");
			}

			@Override
			public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
				return FileVisitResult.CONTINUE;
			}

			@Override
			public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
				return FileVisitResult.CONTINUE;
			}
			
		});
	}

	protected void addSong(Path file) {
		// don't add if it's already in the cache (so has been processed)
		if (!filePaths.contains(file.toString()))
		{
			// and add to the appropriate song, genre, album, and artist
			try
			{
				InputStream input = new FileInputStream(file.toFile());
				ContentHandler handler = new DefaultHandler();
				Metadata meta = new Metadata();
				Parser parser = new Mp3Parser();
				ParseContext context = new ParseContext();
				parser.parse(input, handler, meta, context);
				input.close();
				
				CacheEntry entry = new CacheEntry(file.toString(),
						meta.get("xmpDM:artist"),
						meta.get("title"),
						meta.get("xmpDM:album"),
						meta.get("xmpDM:genre"));
				
				addEntryToSongList(entry, file.toFile());
				
				cache.getSongs().add(entry);
			}
			catch (Exception ex)
			{
				// can't add this one
				System.err.println("Failed to parse metadata from: " + file.toString() + ", " + ex.getMessage());
			}
		}
	}

	private void addToMap(Map<String, List<String>> map, String key, File file) {
		String lowerKey = key.toLowerCase();
		
		if (!map.containsKey(lowerKey))
		{
			map.put(lowerKey, new ArrayList<String>());
		}
		
		map.get(lowerKey).add(file.getAbsolutePath());
	}

	public List<String> getList(String listSelector, String listKey) {
		if (listSelector.equals("genre"))
		{
			return genres.get(listKey);
		}
		else if (listSelector.equals("artist"))
		{
			return artists.get(listKey);
		}
		else if (listSelector.equals("album"))
		{
			return albums.get(listKey);
		}
		else if (listSelector.equals("song"))
		{
			return songs.get(listKey);
		}
		else
		{
			return Collections.emptyList();
		}
	}

	private void loadFromCache(String cacheFile) {
		loadCacheDataFromFile(cacheFile);
		processCache();
	}

	private void loadCacheDataFromFile(String cacheFile) {
		try
		{
			File f = new File(cacheFile);
			
			if (f.exists())
			{
				JAXBContext context = JAXBContext.newInstance(CacheList.class);
				Unmarshaller marsh = context.createUnmarshaller();
				
				try (FileInputStream fis = new FileInputStream(f))
				{
					cache = (CacheList) marsh.unmarshal(fis);
				}
			}
			else
			{
				System.err.println("No cache file exists: " + cacheFile);
				cache = new CacheList();
			}
		}
		catch (Exception ex)
		{
			System.err.println("Failed to load data from cache: " + ex.getMessage());
		}
	}

	/**
	 * Goes through each entry in the cache. Adds the data to our maps.
	 * If the file does not exist, delete from the cache.
	 */
	private void processCache() {
		ListIterator<CacheEntry> iter = cache.getSongs().listIterator();
		while (iter.hasNext())
		{
			CacheEntry entry = iter.next();
			
			File f = new File(entry.getPath());
			
			if (f.exists())
			{
				addEntryToSongList(entry, f);
			}
			else
			{
				iter.remove();
			}
		}
	}

	private void addEntryToSongList(CacheEntry entry, File f) {
		filePaths.add(entry.getPath());
		addToMap(artists, entry.getArtist(), f);
		addToMap(songs, entry.getTitle(), f);
		addToMap(albums, entry.getAlbum(), f);
		addToMap(genres, entry.getGenre(), f);
	}

	private void writeCache(String cacheFile) {
		try
		{
			JAXBContext context = JAXBContext.newInstance(CacheList.class);
			Marshaller marsh = context.createMarshaller();
			
			File f = new File(cacheFile);
			if (f.exists()) f.delete();
			
			marsh.marshal(cache, f);
		}
		catch (Exception ex)
		{
			System.err.println("Failed to load data from cache: " + ex.getMessage());
		}
	}

}
