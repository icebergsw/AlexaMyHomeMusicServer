package com.iceberg.alexa.music.server.player;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.*;

import com.iceberg.alexa.music.server.Configuration;
import com.iceberg.alexa.music.server.MusicPlayer;

/**
 * This CorePlayer is a default implementation of an Alexa skill My Home Server
 * music player. The server application is intended to be flexible to allow for
 * a variety of players to be configured in. The CorePlayer is a built-in player
 * that doesn't work with any other home music service, but just stores and
 * plays a list of songs based on reading them from the local machine (per
 * configuration).
 *
 */
public class CorePlayer implements MusicPlayer {

	Random mRandom = new Random();
	SongList songs;
	boolean isPlayingRandom = false;
	LinkedList<File> playQueue = new LinkedList<File>();
	
	/**
	 * Current sequence is always the sequence number of the item currently playing.
	 * It has the offset already adjusted into it.
	 */
	Integer currentSequence = new Integer(1);
	
	/**
	 * When we get a next or previous command, the sequence number starts over (as if the
	 * next or previous song were 1. So this is used to provide an offset to what
	 * the actual location is, had it kept going correctly.
	 */
	int sequenceOffset = 0;
	
	/**
	 * holds a sequence of songs by key (an integer token of sequence) and value (the file)
	 * if the user selects previous or next, we can use this queue to track the songs
	 * and it handles "next" even though the next song is already queued and downloaded
	 * but not playing yet
	 * 
	 * when we go to play a song, if the sequence is in this queue, we use that song.
	 * If it's not, we find a song and add it (if we're playing random).
	 */
	Map<Integer, File> sequenceQueue = new HashMap<Integer, File>();
		
	@Override
	public void initialize(Configuration conf) {
		System.out.println("CorePlayer initializing...");
		songs = SongList.getInstance(conf);
		System.out.println("CorePlayer finished initializing.");
	}

	@Override
	public void playRandom(MusicPlayerCallback cb, Map<String, String> filters) {
		System.out.println("playRandom called");
		for (String key : filters.keySet()) System.out.println(key + " : " + filters.get(key));

		isPlayingRandom = true;
		reset();
		
		playSomethingRandom(cb, getSequenceNumber(filters));
	}

	private void playSomethingRandom(MusicPlayerCallback cb, Integer seq) {
		currentSequence = seq;
		if (sequenceQueue.containsKey(seq))
		{
			playFile(cb, sequenceQueue.get(seq));
		}
		else
		{
			File fileToPlay = songs.getNextRandom();
			sequenceQueue.put(seq, fileToPlay);
			playFile(cb, fileToPlay);
		}
	}

	private void nextCommandCalled(MusicPlayerCallback cb) {
		// current sequence remains the same
		sequenceOffset = currentSequence - 1;
		
		playFile(cb, sequenceQueue.get(currentSequence));
	}

	private void previousCommandCalled(MusicPlayerCallback cb) {
		currentSequence = currentSequence > 2 ? currentSequence - 2 : 1;
		sequenceOffset = currentSequence - 1;
		
		playFile(cb, sequenceQueue.get(currentSequence));
	}

	@Override
	public void playNext(MusicPlayerCallback cb, Map<String, String> filters) {
		System.out.println("playNext called");
		for (String key : filters.keySet()) System.out.println(key + " : " + filters.get(key));

		try
		{
			if (commandGiven(filters))
			{
				processCommand(cb, filters);
			}
			else if (isPlayingRandom)
			{
				playSomethingRandom(cb, getSequenceNumber(filters));
			}
			else
			{
				nextFromQueue(cb, getSequenceNumber(filters));
			}
		}
		catch (Exception ex)
		{
			endOfQueue(cb);
		}
	}

	private boolean commandGiven(Map<String, String> filters) {
		return filters.containsKey("cmd");
	}

	private void processCommand(MusicPlayerCallback cb, Map<String, String> filters) {
		String cmd = filters.get("cmd");
		if ("next".equals(cmd))
		{
			nextCommandCalled(cb);
		}
		else if ("previous".equals(cmd))
		{
			previousCommandCalled(cb);
		}
		else
		{
			// otherwise invalid command, so end
			endOfQueue(cb);
		}
	}

	private void nextFromQueue(MusicPlayerCallback cb, Integer seq) throws IOException {
		if (sequenceQueue.containsKey(seq))
		{
			playFile(cb, sequenceQueue.get(seq));
		}
		else
		{
			if (playQueue.size() > 0)
			{
				File fileToPlay = selectRandomFromPlayQueue();
				sequenceQueue.put(seq, fileToPlay);
				currentSequence = seq;
				playFile(cb, fileToPlay);
			}
			else
			{
				endOfQueue(cb);
			}
		}
	}

	@Override
	public void playFiltered(MusicPlayerCallback cb, Map<String, String> filters) {
		isPlayingRandom = false;
		reset();

		// debug first
		System.out.println("playFiltered :");
		for (String key : filters.keySet()) System.out.println(key + " : " + filters.get(key));
		
		try
		{
			if (filters.containsKey("genre"))
			{
				for (String file : songs.getList("genre", filters.get("genre")))
				{
					playQueue.add(new File(file));
				}
			}
			else if (filters.containsKey("artist"))
			{
				for (String file : songs.getList("artist", filters.get("artist")))
				{
					playQueue.add(new File(file));
				}
			}
			else if (filters.containsKey("album"))
			{
				for (String file : songs.getList("album", filters.get("album")))
				{
					playQueue.add(new File(file));
				}
			}
			else if (filters.containsKey("song"))
			{
				for (String file : songs.getList("song", filters.get("song")))
				{
					playQueue.add(new File(file));
				}
			}
			else
			{
				playRandom(cb, filters);
				return;
			}
			
			nextFromQueue(cb, getSequenceNumber(filters));
		}
		catch (Exception ex)
		{
			System.err.println("Filtered music error: " + ex.getMessage());
		}
	}

	private void reset() {
		sequenceQueue.clear();
		playQueue.clear();
		sequenceOffset = 0;
	}

	private File selectRandomFromPlayQueue() {
		int index = mRandom.nextInt(playQueue.size());
		File file = playQueue.remove(index);
		return file;
	}

	private void playFile(MusicPlayerCallback cb, File fileToPlay) {
		System.err.println("offset: " + sequenceOffset + ", sequence: " + currentSequence + ", song: " + fileToPlay.getName());
		try
		{
			cb.setAudioLength(fileToPlay.length());
			cb.streamFile(fileToPlay);
		}
		catch (Exception ex)
		{
			System.err.println("Play file failed: " + ex.getMessage());
			// do nothing, we just fail
			// but it probably means the server connection is broken, so nothing we could do anyway
		}
	}

	private void endOfQueue(MusicPlayerCallback cb) {
		try
		{
			cb.setAudioLength(-1L);
			OutputStream os = cb.getOutputStream();
			os.close();
		}
		catch (IOException ex)
		{
			System.err.println("End of queue exception: " + ex.getMessage());
			// ignore, we wanted to stop playing anyway
		}
	}

	/**
	 * Assuming we were passed a sequence number, updates that with
	 * the offset to determine the file index to play.
	 * @param filters
	 * @return
	 */
	private Integer getSequenceNumber(Map<String, String> filters) {
		String seqStr = filters.get("seq");
		if (seqStr != null)
		{
			return Integer.parseInt(seqStr) + sequenceOffset;
		}
		else
		{
			return new Integer(1);
		}
	}

}
