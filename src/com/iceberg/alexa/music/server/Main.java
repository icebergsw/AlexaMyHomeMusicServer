package com.iceberg.alexa.music.server;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

/**
 * Main class for the My Home Music desktop server. This is designed
 * to work with the My Home Music application. This is the main class to
 * run on the local user's desktop machine.
 * 
 * See the My Home Music help for more details.
 */
public class Main {

	public static void main(String[] args) throws Exception {
		Configuration conf = loadConfiguration(args);
		MusicServer ms = new MusicServer();
		ms.setConfiguration(conf);
		ms.start();
	}

	private static Configuration loadConfiguration(String[] args) {
		try
		{
			URL configurationFile = findConfigurationFile(args);

			JAXBContext context = JAXBContext.newInstance(Configuration.class);
			Unmarshaller marsh = context.createUnmarshaller();
			
			Configuration conf = (Configuration) marsh.unmarshal(configurationFile);

			return conf;
		}
		catch (Exception ex)
		{
			throw new RuntimeException(ex);
		}
	}

	/**
	 * Checking, in order:
	 * 	command-line: "-config <file>"
	 *  env var: AMS_CONFIG_FILE
	 *  current working directory (amsconfig.xml)
	 *  user home directory (amsconfig.xml)
	 *  
	 * @param args
	 * @return
	 * @throws MalformedURLException 
	 */
	private static URL findConfigurationFile(String[] args) throws MalformedURLException {
		for (int i = 0; i < args.length; i++)
		{
			if (args[i].equals("-config"))
			{
				return new URL(args[i+1]);
			}
		}
		
		if (System.getenv("AMS_CONFIG_FILE") != null) return new File(System.getenv("AMS_CONFIG_FILE")).toURI().toURL();
		
		File cwdFile = new File("amsconfig.xml");
		if (cwdFile.exists()) return cwdFile.toURI().toURL();
		
		// linux home
		File linuxHomeDirFile = new File(System.getenv("HOME"), "amsconfig.xml");
		if (linuxHomeDirFile.exists()) return linuxHomeDirFile.toURI().toURL();
		
		// windows home
		File winHomeDirFile = new File(System.getenv("USERPROFILE"), "amsconfig.xml");
		if (winHomeDirFile.exists()) return winHomeDirFile.toURI().toURL();
		
		throw new RuntimeException("Configuration File amsconfig.xml not found.");
	}

}
